'use strict';

// Angular E2E Testing Guide:
// https://docs.angularjs.org/guide/e2e-testing

describe('Polandball Application', function() {

  it('should redirect `index.html` to `index.html#!/phones', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toBe('/galleries');
  });

  describe('Polandball list', function() {

    beforeEach(function() {
      browser.get('index.html#!/phones');
    });

    it('should filter galleries as a user types into the search box', function() {
      var galleryList = element.all(by.repeater('polandball in $ctrl.polandballs'));
      var query = element(by.model('$ctrl.query'));


      expect(galleryList.count()).toBeGreaterThan(0)

      query.sendKeys('THERE_ARE_NO_SUCH_BALLS_FOR_SURE');
      query.submit();
      expect(galleryList.count()).toBe(0);

      query.clear();
      query.sendKeys('polska');
      query.submit();
      expect(galleryList.count()).toBeGreaterThan(0);
    });

    it('should load more balls after clicking `Next balls`', function() {

      var galleryList = element.all(by.repeater('polandball in $ctrl.polandballs'));
      var countBefore = galleryList.count();

      element.all(by.css('button.btn.btn-default')).first().click();
      expect(galleryList.count()).toBeGreaterThan(countBefore);
    });
  //
  });
  //
  describe('Polandball details', function() {

    beforeEach(function() {
      browser.get('index.html#!/galleries/C6IcSdh');
    });

    it('should display right title for `C6IcSdh` page', function() {
      expect(element(by.binding('$ctrl.polandball.title')).getText()).toBe('Polandball comic');
    });

    it('should display comments for `C6IcSdh` page', function() {
      var comments = element.all(by.repeater('comment in $ctrl.comments'));
      expect(comments.count()).toBeGreaterThan(100);
    });

    it('should display image for `C6IcSdh` page', function() {
      expect(element(by.css('img.img-responsive')).isPresent()).toBe(true);
    });
  });

});
