# Polandball Gallery


## Overview

This application is designed to display Polandball memes using [Imgur API][imgur-api]. The application filesystem layout structure is based on the [angular-phonecat][angular-phonecat] project.

### Using this app user is able to:
- See Polandball images on the main page.
- Search for specific Polandball memes.
- See details of selected Polandball with comments and their details.
- See more Pollandballs of same author.
- Easily share selected Polandball with friends.

## Prerequisites

### Git

- You can find documentation and download git [here][git-home].

### Node.js and Tools

- Get [Node.js][node-download].
- Install the tool dependencies: `npm install`


### Installing Dependencies

The application relies upon various Node.js tools, such as [Bower][bower], [Karma][karma] 
[Protractor][protractor] and [Angular Social Share][angular-socialshare]. You can install these by running:

```
npm install
```

### Running the Application during Development

- Run `npm start`.
- Navigate your browser to [http://localhost:8000/](http://localhost:8000/) to see the application 
- running.

### Running karma tests
- Run `npm test`.

### Running e2e protraktor tests
- Run `npm run protractor`.

[angular-phonecat]: https://github.com/angular/angular-phonecat
[angular-socialshare]: https://github.com/720kb/angular-socialshare
[imgur-api]: http://api.imgur.com/
[bower]: http://bower.io/
[git-home]: https://git-scm.com
[git-setup]: https://help.github.com/articles/set-up-git/
[karma]: https://karma-runner.github.io
[node-download]: https://nodejs.org/en/download/
[protractor]: http://www.protractortest.org/
