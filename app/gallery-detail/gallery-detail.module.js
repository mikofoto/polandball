'use strict';

// Define the `polandballDetail` module
angular.module('galleryDetail', [
    'ngRoute',
    'core.imgur',
    'core.data',
    '720kb.socialshare'
]);
