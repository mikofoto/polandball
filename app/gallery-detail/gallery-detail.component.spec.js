'use strict';

describe('galleryDetail', function () {

    // Load the module that contains the `phoneDetail` component before each test
    beforeEach(module('galleryDetail', function ($provide) {
        $provide.constant('SOCIAL_MEDIA', {
            FACEBOOK: {APP_ID: 'YOUR_APPID'},
        });
        $provide.constant('IMGUR', {
            CLIENT_ID: 'CLIENT_ID',
            HOST: 'https://api.imgur.com'
        });
    }));

    // Test the controller
    describe('galleryDetailController', function () {
        var $httpBackend, ctrl;
        var galleryMock = {
            "data": {
                "id": "KkTLC0t",
                "title": "Freedom is the only way, yeah!",
                "description": null,
                "datetime": 1436024315,
                "type": "image\/gif",
                "animated": true,
                "width": 600,
                "height": 940,
                "size": 287865,
                "views": 2304754,
                "bandwidth": 663458010210,
                "vote": null,
                "favorite": false,
                "nsfw": false,
                "section": "polandball",
                "account_url": "koleye",
                "account_id": 16196991,
                "is_ad": false,
                "in_gallery": true,
                "topic": "Funny",
                "topic_id": 2,
                "gifv": "http:\/\/i.imgur.com\/KkTLC0t.gifv",
                "mp4": "http:\/\/i.imgur.com\/KkTLC0t.mp4",
                "mp4_size": 201677,
                "link": "http:\/\/i.imgur.com\/KkTLC0t.gif",
                "looping": true,
                "comment_count": 377,
                "ups": 12859,
                "downs": 351,
                "points": 12508,
                "score": 13660,
                "is_album": false
            }, "success": true, "status": 200
        };

        var commentsMock = {
            "data": [
                {
                    "id": 441331767,
                    "image_id": "KkTLC0t",
                    "comment": "by our American standards, this is pretty subtle.",
                    "author": "theclassiestmetroid",
                    "author_id": 159130,
                    "on_album": false,
                    "album_cover": null,
                    "ups": 2325,
                    "downs": 14,
                    "points": 2311,
                    "datetime": 1436024511,
                    "parent_id": 0,
                    "deleted": false,
                    "vote": null,
                    "platform": "unknown",
                    "children": []
                }]
        }

        beforeEach(inject(function ($componentController, _$httpBackend_, $routeParams) {
            $httpBackend = _$httpBackend_;
            $httpBackend.expectGET('https://api.imgur.com/3/gallery/image/x0i5qXU').respond(galleryMock);
            $httpBackend.expectGET('https://api.imgur.com/3/gallery/x0i5qXU/comments/best').respond(commentsMock);

            $routeParams.galleryId = 'x0i5qXU';

            ctrl = $componentController('galleryDetail');
        }));

        it('Should fetch gallery and comments data', function () {
            expect(ctrl.polandball).toEqual(undefined);
            expect(ctrl.comments).toEqual(undefined);
            $httpBackend.flush();
            expect(ctrl.polandball.title).toEqual('Freedom is the only way, yeah!');
            expect(ctrl.comments[0].id).toEqual(441331767);
        });
    });
});