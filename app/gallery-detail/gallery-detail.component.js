'use strict';

angular.module('galleryDetail')
    .component('galleryDetail', {
        templateUrl: 'gallery-detail/gallery-detail.template.html',
        controller: ['$routeParams', '$location', 'Gallery', 'Data', 'Comments', 'Socialshare',
            function galleryDetailController($routeParams, $location, Gallery, Data, Comments, Socialshare) {
                var self = this;
                self.pageAddress = $location.absUrl();
                self.limitComments = 10;
                self.setImage = function setImage(imageUrl) {
                    self.mainImageUrl = imageUrl;
                };

                self.share = function(providerName) {
                    var url = self.pageAddress;
                    var tags = 'polandball';
                    var shareConfig = {
                        'provider': providerName,
                        'attrs': {
                            'socialshareUrl': url
                        }
                    };

                    switch(providerName) {
                        case 'facebook':
                            Socialshare.share(shareConfig);
                            break;
                        case 'twitter':
                            shareConfig.attrs.socialshareHashtags = tags;
                            Socialshare.share(shareConfig);
                            break;
                        case 'google':
                            Socialshare.share(shareConfig);
                            break;
                    }
                };
                self.loadNextComments = function() {
                    self.limitComments = self.limitComments + 10;
                };
                if (!Data.hasGalleries()) {
                    Gallery.get({id: $routeParams.galleryId}, function (result) {
                        self.polandball = result.data;
                        self.setImage(self.polandball.link);
                        Data.setGalleries([self.polandball]);
                    });
                } else {
                    self.polandball = Data.getGallery($routeParams.galleryId);
                    self.setImage(self.polandball.link);

                }

                Comments.get({id: $routeParams.galleryId}, function (result) {
                    self.comments = result.data;
                });
            }
        ]
    });