'use strict';

describe('galleryList', function () {

    // Load the module that contains the `galleryList` component before each test
    beforeEach(module('galleryList', function ($provide) {
        $provide.constant('IMGUR', {
            CLIENT_ID: 'CLIENT_ID',
            HOST: 'https://api.imgur.com'
        });
    }));
    //
    // Test the controller
    describe('galleryListController', function () {
        var $httpBackend, ctrl;

        var galleriesMock = {
            "data": [
                {
                    "id": "KkTLC0t",
                    "title": "Freedom is the only way, yeah!",
                    "description": null,
                    "datetime": 1436024315,
                    "type": "image\/gif",
                    "animated": true,
                    "width": 600,
                    "height": 940,
                    "size": 287865,
                    "views": 2304754,
                    "bandwidth": 663458010210,
                    "vote": null,
                    "favorite": false,
                    "nsfw": false,
                    "section": "polandball",
                    "account_url": "koleye",
                    "account_id": 16196991,
                    "is_ad": false,
                    "in_gallery": true,
                    "topic": "Funny",
                    "topic_id": 2,
                    "gifv": "http:\/\/i.imgur.com\/KkTLC0t.gifv",
                    "mp4": "http:\/\/i.imgur.com\/KkTLC0t.mp4",
                    "mp4_size": 201677,
                    "link": "http:\/\/i.imgur.com\/KkTLC0t.gif",
                    "looping": true,
                    "comment_count": 377,
                    "ups": 12859,
                    "downs": 351,
                    "points": 12508,
                    "score": 13660,
                    "is_album": false
                },
                {
                    "id": "XEvhHtd",
                    "title": "The Good Old Days",
                    "description": null,
                    "datetime": 1409306836,
                    "type": "image\/png",
                    "animated": false,
                    "width": 700,
                    "height": 8031,
                    "size": 252816,
                    "views": 1786810,
                    "bandwidth": 451734156960,
                    "vote": null,
                    "favorite": false,
                    "nsfw": false,
                    "section": "polandball",
                    "account_url": "arrrrrrz",
                    "account_id": 12467188,
                    "is_ad": false,
                    "in_gallery": true,
                    "topic": null,
                    "topic_id": 0,
                    "link": "http:\/\/i.imgur.com\/XEvhHtd.png",
                    "comment_count": 682,
                    "ups": 12207,
                    "downs": 194,
                    "points": 12013,
                    "score": 13223,
                    "is_album": false
                }]
        };
        var galleriesSearchMock = {
            "data": [
                {
                    "id": "XEvhHtd",
                    "title": "The Good Old Days",
                    "description": null,
                    "datetime": 1409306836,
                    "type": "image\/png",
                    "animated": false,
                    "width": 700,
                    "height": 8031,
                    "size": 252816,
                    "views": 1786810,
                    "bandwidth": 451734156960,
                    "vote": null,
                    "favorite": false,
                    "nsfw": false,
                    "section": "polandball",
                    "account_url": "arrrrrrz",
                    "account_id": 12467188,
                    "is_ad": false,
                    "in_gallery": true,
                    "topic": null,
                    "topic_id": 0,
                    "link": "http:\/\/i.imgur.com\/XEvhHtd.png",
                    "comment_count": 682,
                    "ups": 12207,
                    "downs": 194,
                    "points": 12013,
                    "score": 13223,
                    "is_album": false
                }]
        };

        beforeEach(inject(function ($componentController, _$httpBackend_) {
            $httpBackend = _$httpBackend_;
            $httpBackend.expectGET('https://api.imgur.com/3/gallery/search/top/weak/0/search?q=polandball').respond(galleriesMock);


            ctrl = $componentController('galleryList');
        }));
        it('Should fetch all galleries', function () {
            expect(ctrl.polandballs).toEqual([]);
            $httpBackend.flush();
            expect(ctrl.polandballs.length).toEqual(2);
        });

        it('should set values for the `query` property when searching for user specific polandballs', function () {
            ctrl.searchByUser('arrrrrrz');
            expect(ctrl.currentPage).toBe(0);
            expect(ctrl.polandballs).toEqual([]);
            expect(ctrl.query).toBe('user: arrrrrrz');
        });
    });

});
