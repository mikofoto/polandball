'use strict';

angular.
  module('galleryList').
  component('galleryList', {
    templateUrl: 'gallery-list/gallery-list.template.html',
    controller: ['Galleries', 'Data',
      function galleryListController(Galleries, Data) {
        var self = this;
        self.currentPage = 0;
        self.polandballs = [];
        self.query = Data.getSearch();

        self.loadNext = function() {
          Galleries.get({window: 'weak', page: self.currentPage, query: self.query ? 'polandball AND ' + self.query : 'polandball'}, function(result) {
            self.currentPage++;
            self.polandballs = self.polandballs.concat(result.data);
            Data.setGalleries(self.polandballs);
            Data.setSearch(self.query);
          });
        }

        self.search = function () {
          self.currentPage = 0;
          self.polandballs = [];
          self.loadNext();
        }

        self.searchByUser = function (userName) {
          self.currentPage = 0;
          self.polandballs = [];
          self.query = 'user: ' + userName;
          self.loadNext();
        }
        this.loadNext();
      }

    ]
  });
