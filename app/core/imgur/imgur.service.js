'use strict';

angular.module('core.imgur').factory('Galleries', ['$resource', 'IMGUR',
    function ($resource, IMGUR) {
        return $resource(IMGUR.HOST + '/3/gallery/search/:sort/:window/:page/search?q=:query', {},
            {
                get: {
                    headers: {Authorization: 'Client-ID ' + IMGUR.CLIENT_ID},
                    method: 'GET',
                    params: {
                        sort: 'top',
                        window: 'all',
                        query: 'polandball',
                        page: 0,
                    },
                    transformResponse: function (result, headersGetter, status) {
                        result = angular.fromJson(result);
                        result.data = result.data.filter(function (item) {
                            if (!item.is_album) return item;
                        });
                        return result;
                    }
                }
            });
    }
]).factory('Gallery', ['$resource', 'IMGUR',
    function ($resource, IMGUR) {
        return $resource(IMGUR.HOST + '/3/gallery/image/:id', {},
            {
                get: {
                    headers: {Authorization: 'Client-ID ' + IMGUR.CLIENT_ID},
                    method: 'GET',
                }
            });
    }
]).factory('Comments', ['$resource', 'IMGUR',
    function ($resource, IMGUR) {
        return $resource(IMGUR.HOST + '/3/gallery/:id/comments/best', {},
            {
                get: {
                    headers: {Authorization: 'Client-ID ' + IMGUR.CLIENT_ID},
                    method: 'GET',
                }
            });
    }
]);