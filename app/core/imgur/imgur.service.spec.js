'use strict';

describe('Gallery', function () {
    var $httpBackend;
    var Gallery;
    var galleryMock = {
        "id": "x0i5qXU",
        "title": "Ancient tradition",
        "description": null,
        "datetime": 1436193680,
        "type": "image\/png",
        "animated": false,
        "width": 1000,
        "height": 4243,
        "size": 104881,
        "views": 1293381,
        "bandwidth": 135651092661,
        "vote": null,
        "favorite": false,
        "nsfw": false,
        "section": "polandball",
        "account_url": null,
        "account_id": null,
        "is_ad": false,
        "in_gallery": true,
        "topic": null,
        "topic_id": 0,
        "link": "http:\/\/i.imgur.com\/x0i5qXU.png",
        "comment_count": 1489,
        "ups": 7759,
        "downs": 258,
        "points": 7501,
        "score": 8735,
        "is_album": false
    };

    // Load the module that contains the `Gallery` service before each test
    beforeEach(module('core.imgur', function ($provide) {
        $provide.constant('IMGUR', {
            CLIENT_ID: 'CLIENT_ID',
            HOST: 'https://api.imgur.com'
        });
    }));

    // Instantiate the service and "train" `$httpBackend` before each test
    beforeEach(inject(function (_$httpBackend_, _Gallery_) {
        $httpBackend = _$httpBackend_;
        $httpBackend.expectGET('https://api.imgur.com/3/gallery/image/x0i5qXU').respond(galleryMock);

        Gallery = _Gallery_;
    }));

    // Verify that there are no outstanding expectations or requests after each test
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should fetch datails for gallery id=x0i5qXU', function () {

        var gallery;

        gallery = Gallery.get({id: 'x0i5qXU'});
        expect(gallery.data).toEqual(undefined);
        $httpBackend.flush();
        expect(gallery.title).toEqual("Ancient tradition");
    });

});
