'use strict';

angular.module('core.data').factory('Data',
    function () {
        var data = {
            galleries: [],
            search: ''
        };

        return {
            getGalleries: function () {
                return data.galleries;
            },
            hasGalleries: function () {
                return data.galleries.length > 0 ? true : false;
            },
            getGallery: function (id) {
                return data.galleries.filter(function (item) {
                    if (item.id == id) return item;
                })[0];
            },
            setGalleries: function (galleries) {
                data.galleries = galleries;
            },

            getSearch: function () {
                return data.search;
            },
            setSearch: function (search) {
                data.search = search;
            }
        };
    }
);
