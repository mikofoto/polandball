'use strict';

describe('Data', function () {
    var Data;
    var galleries = [{
        "id": "PHGveVU",
        "title": "America's New Years Resolution",
        "description": null,
        "datetime": 1425304050,
        "type": "image\/png",
        "animated": false,
        "width": 524,
        "height": 2116,
        "size": 121417,
        "views": 1072969,
        "bandwidth": 130276677073,
        "vote": null,
        "favorite": false,
        "nsfw": false,
        "section": "polandball",
        "account_url": null,
        "account_id": null,
        "is_ad": false,
        "in_gallery": true,
        "topic": null,
        "topic_id": 0,
        "link": "http:\/\/i.imgur.com\/PHGveVU.png",
        "comment_count": 304,
        "ups": 7640,
        "downs": 218,
        "points": 7422,
        "score": 9089,
        "is_album": false
    }, {
        "id": "VmGaK",
        "title": "Relevant",
        "description": null,
        "datetime": 1421386036,
        "cover": "UeYZFfD",
        "cover_width": 1280,
        "cover_height": 720,
        "account_url": "TheColossalTitan",
        "account_id": 16268919,
        "privacy": "public",
        "layout": "blog",
        "views": 421608,
        "link": "http:\/\/imgur.com\/a\/VmGaK",
        "ups": 9296,
        "downs": 336,
        "points": 8960,
        "score": 8960,
        "is_album": true,
        "vote": null,
        "favorite": false,
        "nsfw": false,
        "section": "",
        "comment_count": 548,
        "topic": null,
        "topic_id": 0,
        "images_count": 8,
        "in_gallery": true,
        "is_ad": false
    }, {
        "id": "x0i5qXU",
        "title": "Ancient tradition",
        "description": null,
        "datetime": 1436193680,
        "type": "image\/png",
        "animated": false,
        "width": 1000,
        "height": 4243,
        "size": 104881,
        "views": 1293381,
        "bandwidth": 135651092661,
        "vote": null,
        "favorite": false,
        "nsfw": false,
        "section": "polandball",
        "account_url": null,
        "account_id": null,
        "is_ad": false,
        "in_gallery": true,
        "topic": null,
        "topic_id": 0,
        "link": "http:\/\/i.imgur.com\/x0i5qXU.png",
        "comment_count": 1489,
        "ups": 7759,
        "downs": 258,
        "points": 7501,
        "score": 8735,
        "is_album": false
    }];

    // Add a custom equality tester before each test
    beforeEach(function () {
        jasmine.addCustomEqualityTester(angular.equals);
    });

    // Load the module that contains the `Gallery` service before each test
    beforeEach(module('core.data'));

    // Instantiate the service and "train" `$httpBackend` before each test
    beforeEach(inject(function (_Data_) {
        Data = _Data_;
    }));


    it('There should be no galleries at the beggining', function () {

        expect(Data.hasGalleries()).toEqual(false);
    });

    it('There should be 60 galleries after adding them by setGalleries', function () {

        Data.setGalleries(galleries)
        expect(Data.getGalleries().length).toEqual(3);
    });

    it('There should gallery id=C6IcSdh', function () {

        Data.setGalleries(galleries)
        expect(Data.getGallery('x0i5qXU').title).toEqual('Ancient tradition');
    });

    it('Verify setSearch for TEST_STRING', function () {

        Data.setSearch('TEST_STRING')
        expect(Data.getSearch()).toEqual('TEST_STRING');
    });

});
