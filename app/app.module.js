'use strict';

// Define the `phonecatApp` module
angular.module('polandball', [
  'ngAnimate',
  'ngRoute',
  'core',
  'galleryDetail',
  'galleryList'
]);
