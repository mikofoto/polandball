'use strict';

angular.
  module('polandball').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');
      $routeProvider.
        when('/galleries', {
          template: '<gallery-list></gallery-list>'
        }).
        when('/galleries/:galleryId', {
          template: '<gallery-detail></gallery-detail>'
        }).
      when('/account/:userName/images', {
        template: '<image-list></image-list>'
      }).
        otherwise('/galleries');
    }
  ]).
    constant('IMGUR',{
      CLIENT_ID: '6b463ae35e07648',
      HOST: 'https://api.imgur.com'
    }).
  constant('SOCIAL_MEDIA',{
    FACEBOOK: {APP_ID: 'YOUR_APPID'}
});
